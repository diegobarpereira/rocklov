describe "POST /signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Debora", email: "debora@gmail.com", password: "test123" }
      MongoDB.new.remove_user(payload[:email])

      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      expect(@result.code).to eql 200
    end

    it "valida id do usuario" do
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  context "usuario ja existe" do
    before(:all) do
      payload = { name: "Joao da Silva", email: "joao@gmail.com", password: "test123" }

      Signup.new.create(payload)
      @result = Signup.new.create(payload)
    end

    it "valida status code 409" do
      expect(@result.code).to eql 409
    end

    it "deve retornar mensagem" do
      expect(@result.parsed_response["error"]).to eql "Email already exists :("
    end
  end

  # #name é obrigatorio
  # context "nome obrigatorio" do
  #   before(:all) do
  #     payload = { name: "", email: "joao@gmail.com", password: "test123" }

  #     @result = Signup.new.create(payload)
  #   end

  #   it "valida status code 412" do
  #     expect(@result.code).to eql 412
  #   end

  #   it "deve retornar mensagem" do
  #     expect(@result.parsed_response["error"]).to eql "required name"
  #   end
  # end

  # #email é obrigatorio
  # context "email obrigatorio" do
  #   before(:all) do
  #     payload = { name: "Joao", email: "", password: "test123" }

  #     @result = Signup.new.create(payload)
  #   end

  #   it "valida status code 412" do
  #     expect(@result.code).to eql 412
  #   end

  #   it "deve retornar mensagem" do
  #     expect(@result.parsed_response["error"]).to eql "required email"
  #   end
  # end
  # #password é obrigatório
  # context "password obrigatorio" do
  #   before(:all) do
  #     payload = { name: "Joao", email: "joao@bol.com", password: "" }

  #     @result = Signup.new.create(payload)
  #   end

  #   it "valida status code 412" do
  #     expect(@result.code).to eql 412
  #   end

  #   it "deve retornar mensagem" do
  #     expect(@result.parsed_response["error"]).to eql "required password"
  #   end

  examples = [
    {
      title: "nome obrigatorio",
      payload: { nome: "", email: "joao@bol.com", password: "123456" },
      code: 412,
      error: "required name",
    },
    {
      title: "sem o campo nome",
      payload: { email: "joao@bol.com", password: "123456" },
      code: 412,
      error: "required name",
    },
    {
      title: "email em branco",
      payload: { name: "Joao", email: "", password: "pwd123" },
      code: 412,
      error: "required email",
    },
    {
      title: "email invalido",
      payload: { name: "Joao", email: "joao&gmail.com", password: "pwd123" },
      code: 412,
      error: "wrong email",
    },
    {
      title: "sem o campo email",
      payload: { name: "Joao", password: "pwd123" },
      code: 412,
      error: "required email",
    },
    {
      title: "password obrigatorio",
      payload: { name: "Joao", email: "joao@gmail.com", password: "" },
      code: 412,
      error: "required password",
    },
    {
      title: "sem o campo password",
      payload: { name: "Joao", email: "joao@gmail.com" },
      code: 412,
      error: "required password",
    },
  ]

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Signup.new.create(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "valida mensagem de erro #{e[:error]}" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
